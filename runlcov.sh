# test group names defined in the test project

# run all tests at once, much faster because coverage data is collected only once.
RUNALL=1

if [ $RUNALL = 0 ]; then
    TESTS=""

    for TST in $TESTS; do
        echo 
        echo Run test $TST ...
        lcov --zerocounters --directory build
        # run specified test group
        ./dist/ImcDemo.exe -c -v -g $TST
        # collect data
        rm build/$TST.info
        lcov --capture --directory build --output-file build/$TST.info --test-name $TST
    done
else
    echo Run all tests ...
    lcov --zerocounters --directory build
    # run all tests
    cd dist/
    ./ImcDemo.exe -c -v -ojunit
    cd ..
    # collect data
    rm -f build/*.info
    lcov --capture --directory build --output-file build/alltests.info --test-name alltests
fi

# find lcov output files
#TEST_OUT=$(ls build/*.info)

sh filter_lcov_report.sh
TEST_OUT=build/alltests_filtered.info

echo 
echo Generating HTML ...
genhtml $TEST_OUT --output-directory lcov_output \
        --title "IMC unit test coverage" \
        --show-details --legend --num-spaces 4 --highlight

