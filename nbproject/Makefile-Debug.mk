#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=mingw-w64-i686-gcc-7.2-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/crc16.o \
	${OBJECTDIR}/src/imc.o \
	${OBJECTDIR}/src/utils/hmi_regs.o \
	${OBJECTDIR}/src/utils/rprintf.o \
	${OBJECTDIR}/src/utils/rscanf.o \
	${OBJECTDIR}/src/utils/trace.o \
	${OBJECTDIR}/tests/app_main.o \
	${OBJECTDIR}/tests/test_imc.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/tests/test_main.o

# C Compiler Flags
CFLAGS=-O0 -g2 -Wall -Wshadow --coverage

# CC Compiler Flags
CCFLAGS=-O0 -Wall --coverage
CXXFLAGS=-O0 -Wall --coverage

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=cpputest-3.7.2/dist/libcpputest-3.7.2.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/ImcDemo.exe

${CND_DISTDIR}/ImcDemo.exe: cpputest-3.7.2/dist/libcpputest-3.7.2.a

${CND_DISTDIR}/ImcDemo.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}
	${LINK.cc} -o ${CND_DISTDIR}/ImcDemo.exe ${OBJECTFILES} ${LDLIBSOPTIONS} --coverage -pg

${OBJECTDIR}/src/crc16.o: src/crc16.c
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/crc16.o src/crc16.c

${OBJECTDIR}/src/imc.o: src/imc.c
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/imc.o src/imc.c

${OBJECTDIR}/src/utils/hmi_regs.o: src/utils/hmi_regs.c
	${MKDIR} -p ${OBJECTDIR}/src/utils
	${RM} "$@.d"
	$(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/utils/hmi_regs.o src/utils/hmi_regs.c

${OBJECTDIR}/src/utils/rprintf.o: src/utils/rprintf.c
	${MKDIR} -p ${OBJECTDIR}/src/utils
	${RM} "$@.d"
	$(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/utils/rprintf.o src/utils/rprintf.c

${OBJECTDIR}/src/utils/rscanf.o: src/utils/rscanf.c
	${MKDIR} -p ${OBJECTDIR}/src/utils
	${RM} "$@.d"
	$(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/utils/rscanf.o src/utils/rscanf.c

${OBJECTDIR}/src/utils/trace.o: src/utils/trace.c
	${MKDIR} -p ${OBJECTDIR}/src/utils
	${RM} "$@.d"
	$(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/utils/trace.o src/utils/trace.c

${OBJECTDIR}/tests/app_main.o: tests/app_main.cpp
	${MKDIR} -p ${OBJECTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -Icpputest-3.7.2/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tests/app_main.o tests/app_main.cpp

${OBJECTDIR}/tests/test_imc.o: tests/test_imc.cpp
	${MKDIR} -p ${OBJECTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -Icpputest-3.7.2/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tests/test_imc.o tests/test_imc.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f1: ${TESTDIR}/tests/test_main.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS}   


${TESTDIR}/tests/test_main.o: tests/test_main.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -Icpputest-3.7.2/include -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/test_main.o tests/test_main.cpp


${OBJECTDIR}/src/crc16_nomain.o: ${OBJECTDIR}/src/crc16.o src/crc16.c 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/crc16.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/crc16_nomain.o src/crc16.c;\
	else  \
	    ${CP} ${OBJECTDIR}/src/crc16.o ${OBJECTDIR}/src/crc16_nomain.o;\
	fi

${OBJECTDIR}/src/imc_nomain.o: ${OBJECTDIR}/src/imc.o src/imc.c 
	${MKDIR} -p ${OBJECTDIR}/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/imc.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/imc_nomain.o src/imc.c;\
	else  \
	    ${CP} ${OBJECTDIR}/src/imc.o ${OBJECTDIR}/src/imc_nomain.o;\
	fi

${OBJECTDIR}/src/utils/hmi_regs_nomain.o: ${OBJECTDIR}/src/utils/hmi_regs.o src/utils/hmi_regs.c 
	${MKDIR} -p ${OBJECTDIR}/src/utils
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/utils/hmi_regs.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/utils/hmi_regs_nomain.o src/utils/hmi_regs.c;\
	else  \
	    ${CP} ${OBJECTDIR}/src/utils/hmi_regs.o ${OBJECTDIR}/src/utils/hmi_regs_nomain.o;\
	fi

${OBJECTDIR}/src/utils/rprintf_nomain.o: ${OBJECTDIR}/src/utils/rprintf.o src/utils/rprintf.c 
	${MKDIR} -p ${OBJECTDIR}/src/utils
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/utils/rprintf.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/utils/rprintf_nomain.o src/utils/rprintf.c;\
	else  \
	    ${CP} ${OBJECTDIR}/src/utils/rprintf.o ${OBJECTDIR}/src/utils/rprintf_nomain.o;\
	fi

${OBJECTDIR}/src/utils/rscanf_nomain.o: ${OBJECTDIR}/src/utils/rscanf.o src/utils/rscanf.c 
	${MKDIR} -p ${OBJECTDIR}/src/utils
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/utils/rscanf.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/utils/rscanf_nomain.o src/utils/rscanf.c;\
	else  \
	    ${CP} ${OBJECTDIR}/src/utils/rscanf.o ${OBJECTDIR}/src/utils/rscanf_nomain.o;\
	fi

${OBJECTDIR}/src/utils/trace_nomain.o: ${OBJECTDIR}/src/utils/trace.o src/utils/trace.c 
	${MKDIR} -p ${OBJECTDIR}/src/utils
	@NMOUTPUT=`${NM} ${OBJECTDIR}/src/utils/trace.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -std=c11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/utils/trace_nomain.o src/utils/trace.c;\
	else  \
	    ${CP} ${OBJECTDIR}/src/utils/trace.o ${OBJECTDIR}/src/utils/trace_nomain.o;\
	fi

${OBJECTDIR}/tests/app_main_nomain.o: ${OBJECTDIR}/tests/app_main.o tests/app_main.cpp 
	${MKDIR} -p ${OBJECTDIR}/tests
	@NMOUTPUT=`${NM} ${OBJECTDIR}/tests/app_main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -Icpputest-3.7.2/include -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tests/app_main_nomain.o tests/app_main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/tests/app_main.o ${OBJECTDIR}/tests/app_main_nomain.o;\
	fi

${OBJECTDIR}/tests/test_imc_nomain.o: ${OBJECTDIR}/tests/test_imc.o tests/test_imc.cpp 
	${MKDIR} -p ${OBJECTDIR}/tests
	@NMOUTPUT=`${NM} ${OBJECTDIR}/tests/test_imc.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -w -DIMC_CONFIG_TEST_RETRANS=1 -DIMC_CONFIG_USE_EVENTS=1 -Isrc -Isrc/utils -Icpputest-3.7.2/include -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tests/test_imc_nomain.o tests/test_imc.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/tests/test_imc.o ${OBJECTDIR}/tests/test_imc_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
