/*******************************************************************************
 * \file    imc_cfg.h
 *
 * \brief   IMC configuration file
 *
 * \author  Mariusz Midor
 *          http://egolhmi.eu
 *          https://bitbucket.org/mmidor/imc
 *
 ******************************************************************************/

#ifndef _IMC_CFG_H_
#define _IMC_CFG_H_

////////////////////////////////////////////////////////////////////////////////
/// includes

#include "types.h"
#include "crc16.h"
#include <assert.h>

////////////////////////////////////////////////////////////////////////////////
/// defines, macros

/*******************************************************************************
 * IMC configuration
 * more information in imc.h "Configuration Guide"
 */

// number of receive buffers for complete frames, minimum 1
#define IMC_CONFIG_RX_BUFF_COUNT                2

// max data bytes in each frame, must be less or eq IMC_DATASIZE_MAX
#define IMC_CONFIG_DATA_SIZE                    500

// acknowledge timeout for sent packet, in SM intervals (see IMC_SM_INTERVAL() macro)
# define IMC_CONFIG_ACK_TIMEOUT                 100

// next byte of frame timeout, in SM intervals
#define IMC_CONFIG_BYTE_TIMEOUT                 30

// use IMC_CRC16 macro to calculate frame checksum value
#ifndef IMC_CONFIG_USE_CRC
# define IMC_CONFIG_USE_CRC                     true
#endif

// configure as receive-only - transmit functions unavailable, so entire code will be much smaller
#ifndef IMC_CONFIG_RECEIVE_ONLY
# define IMC_CONFIG_RECEIVE_ONLY                false
#endif

// number of retransmissions if case of acknowledge timeout detected
#ifndef IMC_CONFIG_TX_RETRANS
# define IMC_CONFIG_TX_RETRANS                  2
#endif

// call IMC_Event() for debug purposes
#ifndef IMC_CONFIG_USE_EVENTS
# define IMC_CONFIG_USE_EVENTS                  false
#endif

// macro used to calculate frame crc16 value
#define IMC_CRC16(p_crc16, p_data, datasize)    CRC16_Compute(p_crc16, p_data, datasize)

// assertion: basic approach
#define IMC_ASSERT(expr)                        assert(expr)

// assertion: alternative solution using user implemented handler to avoid program abort
//extern int imc_assert_failure(const char *expression, const char *file, unsigned int line);
//#define IMC_ASSERT(expr)                        (void) ((!!(expr)) || (imc_assert_failure(#expr, __FILE__, __LINE__)))

//------------------------------------------------------------------------------

#endif
