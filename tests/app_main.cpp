/*******************************************************************************
 * \file    app_main.cpp
 *
 * \brief   Main file
 *
 * \author  Mariusz Midor
 *          http://egolhmi.eu
 *          https://bitbucket.org/mmidor/imc
 *
 ******************************************************************************/

#include "CppUTest/CommandLineTestRunner.h"
#include <vector>

// -----------------------------------------------------------------------------

// wyjście makr TRACE_
extern "C"
void TRACE_PutChar(char C)
{
    // wysłanie znaku terminala przez UART
    // UART1_WriteChar(C);
}

// używane tylko gdy teksty mają być zapisywane do pliku; zależy od TRACE_LogLevel
extern "C"
void TRACE_OpenLog(const char *Path)
{}

extern "C"
void TRACE_CloseLog(void)
{}

extern "C"
void TRACE_FlushLog(void)
{}

// -----------------------------------------------------------------------------

int main(int argc, char** argv)
{
    std::vector<const char*> args(argv, argv + argc);
    args.push_back("-v"); // Verbose output (mandatory!)
    args.push_back("-c"); // Colored output (optional)

//    args.push_back("-n");
//    args.push_back("HdrChecksumError");

    int rc = CommandLineTestRunner::RunAllTests(args.size(), &args[0]);
    return rc;
}

// -----------------------------------------------------------------------------

